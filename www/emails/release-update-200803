Subject: Release Update: Release numbering, goals, armel architecture, BSPs
To: debian-devel-announce@lists.debian.org

Hi,

We are back to the monthly release updates. Let's see what news there is
to tell you for March...


Release goals
~~~~~~~~~~~~~
After adding the g77 to gfortran transition to the list and deferring the
"packages have to build correctly in dirty chroots" goal to lenny+1, we've
decided to freeze the release goal list [1]. We've also changed the format
of the published list to something machine parseable for easier data
mining.

We've seen quite promising progress with some of our goals:

 * debmake: Done.

 * python2.5 transition: There have been filed quite a few bugs
   about differences in memory management between python2.4 and python2.5.
   These are usually easily patched.

 * gcc-4.3 transition: The switch to gcc-4.3 as default compiler
   will happen soon (see below), so all of these bugs will become release
   critical.

 * UTF8 in control files: Most of these have been fixed now and are just
   waiting for transition to testing.

 * LSB headers in init scripts: Many packages have been updated and
   running a dependency based init system now works for most use cases.
   However, there are still a lot of bugs open against fringe packages.

 * dash: Build problems with /bin/dash as default shell have almost all
   been solved. A new round of bugs were filed about bashisms in /bin/sh
   scripts. You are invited to squash these at the BSP in April.

 * piuparts-clean archive, doublebuild: These release goals could
   profit from more attention.


armel architecture
~~~~~~~~~~~~~~~~~~
Packages built for the armel architecture are now available from
ftp.debian.org. Testing migrations scripts have been considering armel for
some days now as non-blocking architecture. We hope that lenny/armel will
soon converge to proper lenny, but the final decision on inclusion of
armel in the set of release architectures has *not* yet been completed.


Release schedule
~~~~~~~~~~~~~~~~
There haven't been any changes in our release schedule.
Please note that we want to release lenny in *6 months*, so only upload
software that you can (and will) stabilize until then.
Please refer to our last release update [2] for a full schedule. Note that
this means we are entering the very soft freeze now. 


BSP Marathon
~~~~~~~~~~~~
At time of writing, we have 460 open RC bugs, which is 460 too many.  A
coordinated effort is needed to reduce this number, so we've decided to
resurrect last year's very successful BSP marathons. As a reminder, we
still have a 0-day NMU policy in effect.

Please note that in a BSP, you shouldn't just NMU every RC bug you see.
While you are working on a package, check for other low-hanging fruits
(like translation updates, typos that can easily be fixed, ...) and fix
them in your NMU. On the other hand, if you notice that a package looks
unmaintained, refrain from fixing the bugs for now and try to find out if
the package should be removed or adopted by another maintainer instead.

To give our BSPs a more targetted feeling, we want to assign one group of
RC bugs and one release goal to each weekend:

BSP on weekend 2008-03-07 to 2008-03-09
---------------------------------------
 + Fix release critical FTBFS bugs detected QA archive rebuilds [3]
 + Fix python2.5 and gcc-4.3 migration problems [4] [5]
 + Do first upgrade tests and report bugs

BSP on weekend 2008-04-04 to 2008-04-06
---------------------------------------
 + Fix remaining FTBFS bugs
 + Fix problems around the init system [6] [7]
 + Check debian-installer and the installation process
 + Start working on release and upgrade notes, collect items that need to
   mentioned in these documents

BSP on weekend 2008-05-01 to 2008-05-04
---------------------------------------
 + Fix piuparts problems [8]
 + Mass upgrade tests

At the moment, we still haven't found hosts for all real life BSPs - so
please check out if you can organize something. Please use [9] for
coordination! There is also a small howto [10] with facts to think about
before organizing a real life BSP.


Package team news
~~~~~~~~~~~~~~~~~
* gcc: The default compiler version will be switched to 4.3 in the next
   few days. No big changes for C packages are expected, but the C++
   header files have seen a thorough dependency cleanup. Some C++ packages
   will fail to build due to headers which now need to be included
   explicitly.

* Iceweasel/Firefox and other Mozilla stuff.
   A xulrunner 1.9 should be uploaded to Debian quite soon, either as a
   new source package in unstable or as a xulrunner update in
   experimental. This will probably happen at the same time xulrunner
   1.9 beta4 is released.

   As other news, the mozilla-stuff packaging team plans for lenny to
   push iceweasel 3 (based on xulrunner) to ease the security team work.
   Iceweasel-3 betas could be uploaded to experimental at some point in
   a not so far future, depending on the team available time.

   The Team's plans with respect to iceape and icedove are unclear at the
   moment as it's unsure which new upstream releases will happen in time
   for lenny.

* KDE:
   There may be a possibility to include KDE4 in lenny. The efforts on
   KDE4.1 have been quite promising and seem to be leading to a desktop
   environment which can fully replace KDE3. The KDE team will provide
   betas and release candidates of the 4.1 release in experimental. Anyone
   interested is encourage to try them out and file any bugs found.
   
   In the event that KDE4.1 is on time, and there are no major issues, an
   upload to unstable in order to include it in lenny is possible. The
   actual decision has been deferred for now.


Tricks from the Release Team
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
One might want to look at the 'Buildd information page' [11] when one is
unsure about the wanna-build status of a package [12] or about the failure
message in a build log.


Lenny version
~~~~~~~~~~~~~
For reading this far, you receive the small reward of the knowledge that
Lenny will be shipped as Debian 5.0.


Cheers,
NN
-- 
http://release.debian.org
Debian Release Team

Footnotes:
[1]   http://release.debian.org/lenny-goals.txt
[2]   http://release.debian.org/emails/release-update-200801 
[3]   http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-qa@lists.debian.org&tag=qa-ftbfs&dist=testing
[4]   http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=goal-python2.5&dist=testing
[5]   http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=tbm@cyrius.com&tag=ftbfs-gcc-4.3&dist=testing
[6]   http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=initscripts-ng-devel@lists.alioth.debian.org&dist=testing
[7]   http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=goal-dash&dist=testing
[8]   http://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-release@lists.debian.org&tag=piuparts-stable-upgrade&dist=testing
[9]   http://wiki.debian.org/BSPMarathon
[10]  http://wiki.debian.org/HostingBSP 
[11]  http://buildd.debian.org/~jeroen/status/
[12]  http://www.debian.org/devel/buildd/wanna-build-states

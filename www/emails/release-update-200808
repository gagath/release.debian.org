Subject: Release Update: freeze guidelines, testing, BSP, rc bug fixes
To: debian-devel-announce@lists.debian.org

Heya,

We are happy to publish yet another issue of our highly successful 
motivational status updates. This month's issue contains, as reward for
your continued interest, the name for lenny's successor.

Freeze status
~~~~~~~~~~~~~
Lenny has been frozen for some time now, stabilizing the package
list. To continue our release efforts, some exceptions for packages
that were waiting in the NEW queue/uploaded shortly before the freeze
are dropped. From now on, only the following rules apply:

 A new version may only contain changes falling in one of the
 following categories (compared to the version in testing):
  - fixes for release critical bugs (i.e., bugs of severity critical,
    grave, and serious) in all packages;
  - changes for release goals, if they are not invasive;
  - fixes for severity: important bugs in packages of priority: optional
    or extra, only when this can be done via unstable;
  - translation updates
  - documentation fixes

Please upload packages fitting this description to unstable, then
request the freeze exception by mail to debian-release@lists.debian.org.
You don't need to include the full diff (which we re-generate from the
uploaded packages anyway), but please include the relevant changelog
entries.

For further information on freeze exceptions, refer to our freeze
announcement [RM:FA], but note that the rules are a bit stricter now.


Upgrade and Install tests
~~~~~~~~~~~~~~~~~~~~~~~~~
With the recent migration of Linux 2.6.26 to lenny, we don't expect any
more disruptive changes to lenny. Thus, we would like to request more
intensive tests of upgrades from etch to lenny and fresh installations.

The Debian Installer team is currently preparing the first (and hopefully
final) release candidate of the lenny installer. If you want to help to
identify bugs, check the Debian Installer Website [DI:WEB] and try the daily
snapshots.

If possible, try to use Linux 2.6.26 on your box and report any problem
back to our bug tracking system. Problems not reported in the next weeks
can't be fixed in the lenny kernel.


Release critical bugs, Removals, BSP
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The release team is, as always, concerned about the number of release
critical bugs affecting testing. We are still optimistic that currently
known issues can be squashed in short time with your help. Fringe
packages with open RC bugs will be removed in the coming weeks. Use the
``rc-alert'' script from the devscripts package to identify removal
candidates that you use.

To fix as many issues as possible, we are inviting people to join us
in a bug squashing party on the next weekend (5th to 6th September).
We will coordinate our efforts in the #debian-bugs IRC channel on
irc.debian.org.


Release notes
~~~~~~~~~~~~~
There is still quite a lot of work to be done on the lenny release notes.
Coordination for this will happen on the debian-doc@lists.debian.org
mailing list (further information to appear in a mail to that list). If
you know of any issues that need to be documented, file them as bugs
against the ``release-notes'' pseudo package.

While you are pondering noteworthy things, feel free to document important
improvements, newly included packages and similiar things on the
NewInLenny page in the Debian Wiki [DW:NIL].

Release name
~~~~~~~~~~~~
We will continue to use Toy Story character names for lenny's successor,
which will be called ``squeeze'' (three-eyed space alien).

Cheers

Luk
-- 
http://release.debian.org
Debian Release Team

References:
 [RM:FA] http://lists.debian.org/debian-devel-announce/2008/07/msg00007.html
 [DI:WEB] http://www.debian.org/devel/debian-installer/
 [DW:NIL] http://wiki.debian.org/NewInLenny
